package com.example.appdeteksikantuk;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    private SQLiteHelper dbHelper;

    private EditText usernameEditText;
    private EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        dbHelper = new SQLiteHelper(this);
        usernameEditText = findViewById(R.id.EditUsername);
        passwordEditText = findViewById(R.id.EditPassword);
        Button mButtonRegister = findViewById(R.id.button_signup);
        mButtonRegister.setOnClickListener(v -> {
            Intent registerIntent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(registerIntent);
        });

    }
    public void login(View view) {
        try {
            String username = usernameEditText.getText().toString();
            String password = passwordEditText.getText().toString();

            if (username.isEmpty() || password.isEmpty()) {
                Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show();
            } else {
                User user = dbHelper.getUser(username);
                if (user == null) {
                    throw new IllegalArgumentException("User not found"); // Melempar IllegalArgumentException
                } else if (!password.equals(user.getPassword())) {
                    Toast.makeText(this, "Wrong password", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Login successful", Toast.LENGTH_SHORT).show();
                    Intent loginIntent = new Intent(LoginActivity.this, MainActivity.class);
                    loginIntent.putExtra("username", username);
                    startActivity(loginIntent);
                    finish();
                }
            }
        } catch (IllegalArgumentException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            Log.d("LOGIN", e.getMessage());
        }
    }

}
