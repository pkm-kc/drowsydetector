package com.example.appdeteksikantuk;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import java.io.ByteArrayOutputStream;

public class RegisterActivity extends AppCompatActivity {

    private ActivityResultLauncher<String> launcher;
    private static final int PICK_IMAGE_REQUEST = 1;
    private ImageView profileImageView;
    private SQLiteHelper dbHelper;

    private EditText usernameEditText;
    private EditText passwordEditText;

    private Button mbtnupload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        dbHelper = new SQLiteHelper(this);
        usernameEditText = findViewById(R.id.username_edit_text);
        passwordEditText = findViewById(R.id.password_edit_text);
        mbtnupload = findViewById(R.id.btn_choose_photo);
        profileImageView = findViewById(R.id.profile_picture);

        // Inisiasi launcher untuk memilih gambar dari galeri
        launcher = registerForActivityResult(new ActivityResultContracts.GetContent(), new ActivityResultCallback<Uri>() {
            @Override
            public void onActivityResult(Uri result) {
                profileImageView.setImageURI(result);
            }
        });

        mbtnupload.setOnClickListener(v -> openGallery());
    }

    public void register(View view) {
        String username = usernameEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        // Check if all required fields are filled
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password) || profileImageView.getDrawable() == null) {
            Toast.makeText(this, "Please fill all fields, including the profile picture", Toast.LENGTH_SHORT).show();
            return;
        }

// Mendapatkan byte array dari gambar
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Bitmap bitmap = ((BitmapDrawable) profileImageView.getDrawable()).getBitmap();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

// Validasi panjang minimal 8 karakter
        if (password.length() < 8) {
            Toast.makeText(this, "Password must be at least 8 characters long", Toast.LENGTH_SHORT).show();
            return;
        }

// Validasi apakah password terdiri dari angka dan huruf
        if (!password.matches(".*\\d.*") || !password.matches(".*[a-zA-Z].*")) {
            Toast.makeText(this, "Password must contain both letters and numbers", Toast.LENGTH_SHORT).show();
            return;
        }

// Jika password lulus validasi, lanjutkan dengan menyimpan data pengguna ke dalam database
        SQLiteHelper dbHelper = new SQLiteHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(UserEntry.COLUMN_USERNAME, username);
        values.put(UserEntry.COLUMN_PASSWORD, password);
        values.put(UserEntry.COLUMN_PROFILE_PICTURE, byteArray);
        long newRowId = db.insert(UserEntry.TABLE_NAME, null, values);
        if (newRowId == -1) {
            Toast.makeText(this, "Registration failed", Toast.LENGTH_SHORT).show();
            return;
        }

// Show success message and move to Login activity
        Toast.makeText(this, "Registration successful", Toast.LENGTH_SHORT).show();
        Intent registerIntent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(registerIntent);
        finish();

    }

    private void openGallery() {
        // Menggunakan launcher untuk memilih gambar dari galeri
        launcher.launch("image/*");

    }
}
