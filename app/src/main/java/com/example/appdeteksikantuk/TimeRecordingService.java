package com.example.appdeteksikantuk;

import static com.example.appdeteksikantuk.CameraActivity.CHANNEL_ID;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TimeRecordingService extends Service {
    private Handler mHandler;
    private Runnable mRunnable;
    private SQLiteHelper mDb;

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler();
        mDb = new SQLiteHelper(getApplicationContext());
        startRecordingTime();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    private void startRecordingTime() {
        mRunnable = new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void run() {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DAY_OF_YEAR, -1);
                String yesterday = sdf.format(cal.getTime());
                boolean isDrowsyYesterday = false;
                List<String> drowsinessLogs = mDb.getDrowsinessLogs();
                for (String log : drowsinessLogs) {
                    if (log.contains(yesterday)) {
                        isDrowsyYesterday = true;
                        break;
                    }
                }
                Intent detailIntent = new Intent(TimeRecordingService.this, DrowsinessDetector.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(TimeRecordingService.this, 0, detailIntent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);


                // Tampilkan notifikasi jika drowsinessLogs terdeteksi kemarin
                if (isDrowsyYesterday) {
                    // Menampilkan notifikasi pada halaman DrowsinessDetector
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_notification)
                            .setContentTitle("Riwayat Deteksi Kelelahan Kemarin")
                            .setContentText("Waspada, Anda telah terdeteksi mengantuk Pada jam" + drowsinessLogs)
                            .setPriority(NotificationCompat.PRIORITY_HIGH);
                            builder.setContentIntent(pendingIntent);

                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
                    int notificationId = 1;
                    notificationManager.notify(notificationId, builder.build());
                }

                mHandler.postDelayed(this, 1000); // Rekam waktu setiap detik
            }
        };

        mHandler.postDelayed(mRunnable, 1000); // Mulai merekam waktu
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(mRunnable);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
