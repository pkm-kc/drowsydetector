package com.example.appdeteksikantuk;

import android.provider.BaseColumns;

public class UserEntry implements BaseColumns {
    public static final String TABLE_NAME = "users";
    public static final String COLUMN_USERNAME = "username";
    public static final String COLUMN_PASSWORD = "password";
    public static final String COLUMN_PROFILE_PICTURE = "profil_picture" ;
}

