/*
 * Hak Cipta (C) Faras Ardian - SI 2019 - 19082010045@student.upnjatim.ac.id - 081717530893
 * DrowsyGuard
 * Aplikasi Deteksi Kantuk
 * Versi 1.0.0
 * Hak Cipta (C) Faras Ardian - SI 2019 - 19082010045@student.upnjatim.ac.id - 081717530893. Semua hak dilindungi.
 */
package com.example.appdeteksikantuk;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    private SQLiteHelper dbHelper;
    private ImageView userPhoto;
    private TextView helloMessage;
    private Button detectButton;
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userPhoto = findViewById(R.id.user_photo);
        helloMessage = findViewById(R.id.hello_message);
        detectButton = findViewById(R.id.detect_button);

        username = getIntent().getStringExtra("username");
        helloMessage.setText("Hello " + username + "!");
        SQLiteHelper dbHelper = new SQLiteHelper(this);
        User user = dbHelper.getUser(username);

        byte[] profilePictureBytes = user.getProfilePicture();
        Bitmap profilePictureBitmap = BitmapFactory.decodeByteArray(profilePictureBytes, 0, profilePictureBytes.length);
        userPhoto.setImageBitmap(profilePictureBitmap);


        detectButton.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity.this, DrowsinessDetector.class);
            startActivity(intent);
        });
    }
}

