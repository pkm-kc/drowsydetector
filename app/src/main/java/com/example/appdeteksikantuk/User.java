package com.example.appdeteksikantuk;

public class User {

    private String username;
    private String password;
    private byte[] profilePicture;

    public User(String username, String password, byte[] profilePicture) {
        this.username = username;
        this.password = password;
        this.profilePicture = profilePicture;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public byte[] getProfilePicture() {
        return profilePicture;
    }

}

