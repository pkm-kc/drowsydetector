package com.example.appdeteksikantuk;

import android.content.ContentValues;
import android.content.Context;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class SQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "user.db";
    private static final int DATABASE_VERSION = 3;

    //TABLE USER
    private static final String TABLE_NAME = "users";
    private static final String COLUMN_USERNAME = "username";
    private static final String COLUMN_PASSWORD = "password";
    private static final String COLUMN_PROFILE_PICTURE = "profil_picture";
    // Tabel untuk menyimpan riwayat deteksi mengantuk
    private static final String TABLE_DROWSINESS_HISTORY = "drowsiness_history";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_TIMESTAMP = "timestamp";
    private static final String CREATE_TABLE_USER =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    COLUMN_USERNAME + " TEXT PRIMARY KEY, " +
                    COLUMN_PASSWORD + " TEXT, " +
                    COLUMN_PROFILE_PICTURE + " BLOB)";
    private static final String CREATE_DROWSINESS_HISTORY_TABLE = "CREATE TABLE "
            + TABLE_DROWSINESS_HISTORY + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_TIMESTAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";
    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_USER);
        db.execSQL(CREATE_DROWSINESS_HISTORY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DROWSINESS_HISTORY);
        onCreate(db);
    }

    public void addUser(String username, String password, byte[] profilePicture) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_USERNAME, username);
        values.put(COLUMN_PASSWORD, password);
        values.put(COLUMN_PROFILE_PICTURE, profilePicture);

        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public User getUser(String username) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[]{COLUMN_USERNAME, COLUMN_PASSWORD, COLUMN_PROFILE_PICTURE},
                COLUMN_USERNAME + "=?",
                new String[]{username}, null, null, null, null);

        if (cursor != null && cursor.moveToFirst()) {
            User user = new User(cursor.getString(0), cursor.getString(1), cursor.getBlob(2));
            cursor.close();
            db.close();
            return user;
        } else {
            cursor.close();
            db.close();
            throw new IllegalArgumentException("Username not found, please register first "); // Menambahkan pesan error
        }

    }
    public void addDrowsinessLog(String timestamp) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_TIMESTAMP, timestamp);

        db.insert(TABLE_DROWSINESS_HISTORY, null, values);
        db.close();
        Log.d("DB", "Data berhasil disimpan di dalam database");
    }
    public List<String> getDrowsinessLogs() {
        List<String> drowsinessLogs = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_DROWSINESS_HISTORY;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int columnIndex = cursor.getColumnIndex(COLUMN_TIMESTAMP);
        if (columnIndex >= 0) {
            if (cursor.moveToFirst()) {
                do {
                    String timestamp = cursor.getString(columnIndex);
                    drowsinessLogs.add(timestamp);
                } while (cursor.moveToNext());
            }
        } else {
            Log.e("Error", "Column not found");
        }

        cursor.close();
        db.close();
        Log.d("DB", "Data yang diambil dari database: " + drowsinessLogs);
        return drowsinessLogs;
    }

    public int getDrowsinessLogCount() {
        String countQuery = "SELECT * FROM " + TABLE_DROWSINESS_HISTORY;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
        return count;
    }

}
