package com.example.appdeteksikantuk;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class StartedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_started);
        Button mButtonStarted = findViewById(R.id.btn_started);
        mButtonStarted.setOnClickListener(v -> {
            Intent startedIntent = new Intent(StartedActivity.this, LoginActivity.class);
            startActivity(startedIntent);
        });
    }
}

